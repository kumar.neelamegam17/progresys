package progresys.com.scandatamatrix.NetworkUtils;

import org.json.JSONException;

public interface CallBack {
    public void onProgress();
    public void onResult(String result);
    public void onResultExtra(String result, int id) throws JSONException;
    public void onCancel();
}