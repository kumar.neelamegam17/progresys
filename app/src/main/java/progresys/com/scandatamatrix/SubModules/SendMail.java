package progresys.com.scandatamatrix.SubModules;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import progresys.com.scandatamatrix.Utils.LocalSharedPreference;

import progresys.com.scandatamatrix.CoreModules.Constants;
import progresys.com.scandatamatrix.CoreModules.Login;
import progresys.com.scandatamatrix.NetworkUtils.CallBack;
import progresys.com.scandatamatrix.NetworkUtils.HTTPAsyncTask;
import progresys.com.scandatamatrix.R;
import progresys.com.scandatamatrix.Utils.Mail;

/**
 * Created by Muthukumar Neelamegam on 1/20/2019.
 * ScanDataMatrix
 */
public class SendMail extends AppCompatActivity {

    @BindView(R.id.toolbarMail)
    Toolbar toolbar;
    @BindView(R.id.edt_quantity) AppCompatEditText edtQunatity;
    @BindView(R.id.parent_sendmail)
    LinearLayout parent;

    @BindView(R.id.layout_dynamicdata)
    LinearLayout dynamicLayout;

    //***************************************************************************************
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_sendmail);

        try {

            GetInitialize();


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    String Product_Info="";
    byte[] ImageData = new byte[]{};
    Boolean Image_Updated=false;
    StringBuilder content=new StringBuilder();

    String PRODUCT_ID  ="";
    String EMPLOYEE_ID ="";
    String EMPLOYEE_NAME ="";

    private void GetInitialize() {

        ButterKnife.bind(this);

        sharedPreference = new LocalSharedPreference(SendMail.this);
        PRODUCT_ID = sharedPreference.getValue("PRODUCT_ID");
        EMPLOYEE_ID = sharedPreference.getValue("EMPLOYEE_ID");
        EMPLOYEE_NAME = sharedPreference.getValue("EMPLOYEE_NAME");

        setSupportActionBar(toolbar);
        toolbar.setTitle("Send Email");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        Constants.changeStatusBarColour(this);

        if (getIntent().getExtras() != null) {
            Bundle retain_string = getIntent().getExtras();
            Product_Info = retain_string.getString("DATA_INFO");
            ImageData = retain_string.getByteArray("DATA_IMAGE");
            Image_Updated = retain_string.getBoolean("IMAGE_UPDATED");
        }


        PrepareContent();//Mail Content



    }

    StringBuilder productStringBuilder = new StringBuilder();
    private void PrepareContent() {


        try {
            JSONArray jsonArr = new JSONArray(Product_Info);

            if (jsonArr.length() > 0) {
                productStringBuilder.append("Product Details</br>");
                productStringBuilder.append("*******************</br><table>\n" +
                        "<tbody>");
                for (int i = 0; i < jsonArr.length(); i++) {
                    JSONObject jsonObj = jsonArr.getJSONObject(i);

                    for (Iterator<String> it = jsonObj.keys(); it.hasNext(); ) {
                        Object key = it.next();
                        //based on you key types
                        String keyStr = (String) key;
                        Object keyvalue = jsonObj.get(keyStr);

                        if(!keyStr.equals("ImageData")){
                            productStringBuilder.append("<tr><td>"+ keyStr + "</td><td> : </td><td>" + Constants.CheckString(keyvalue.toString()+"</td></tr>"));
                            DynamicLayout(keyStr, keyvalue.toString());

                        }


                    }
                }
                productStringBuilder.append("</table>\n" +
                        "</tbody>");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void DynamicLayout(String Label, String Value) {
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_item_dynamic, null);
        TextView txtvw1, txtvw2;
        txtvw1 = view.findViewById(R.id.txtvw1);
        txtvw2 = view.findViewById(R.id.txtvw2);
        txtvw1.setText(Constants.CheckString(Label));
        txtvw2.setText(Constants.CheckString(Value));
        dynamicLayout.addView(view);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        else if(item.getItemId() == R.id.action_done)
        {
            if(Integer.parseInt(edtQunatity.getText().toString())>0 && edtQunatity.getText().length()>0)
            {
                SendEmail();
            }else
            {
                Constants.SnackBar(SendMail.this, "Enter valid quantity?", parent, 2);
            }

        }
        else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void SendEmail() {

        try {
            SendEmailAsyncTask email = new SendEmailAsyncTask();
            email.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        this.finish();
    }


    BottomSheetDialog dialog;
    boolean status=false;
    public class SendEmailAsyncTask extends AsyncTask<Void, Void, Boolean> {

        Mail m;
        Login activity;

        public SendEmailAsyncTask() {
        }

        @Override
        protected void onPreExecute() {
            View view = getLayoutInflater().inflate(R.layout.fragment_bottom_sheet_dialog, null);
            dialog = new BottomSheetDialog(SendMail.this);
            dialog.setContentView(view);
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {


            try {

                status = Mail();

                return true;
            } catch (Exception e) {
                e.printStackTrace();
              //  Constants.SnackBar(SendMail.this, "Unexpected error occured.", parent_layout, 2);
              //  Toast.makeText(SendMail.this, "Unexpected error occured.", Toast.LENGTH_SHORT).show();

                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();

            }
            if (status) {

                Toast.makeText(SendMail.this, "Email Sent..", Toast.LENGTH_SHORT).show();
                //SendMail.this.finish();
                Constants.AlertDialog(SendMail.this, "Information", "Request submitted successfully..",
                        R.drawable.ic_done_all_black_24dp, R.color.green_500, true);


            } else {

                Toast.makeText(SendMail.this, "Email failed to send.. \nTry Later..", Toast.LENGTH_SHORT).show();

            }

        }
    }



    String SERVER_USERNAME  = "";
    String SERVER_PASSWORD  = "";
    String FROM = "";
    String TO = "";
    String SUBJECT = "";
    String MESSAGE = "";


    public boolean Mail(){

        try {

            //Push data to server
            SendEnquiryDetails();

            SendMailDetails();

            if (Image_Updated) {
                UpdateProductImage();
            }


                return true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


    public void UpdateProductImage()
    {
        try {

            String API_LOAD_CLASSES = Constants.APPLICATION_API + "PostDetails/updateProductImage";
            JSONObject jsonObject = new JSONObject();

            Constants.Logger(PRODUCT_ID+"/"+EMPLOYEE_ID, SendMail.this);
            jsonObject.put("ProductID", PRODUCT_ID);
            jsonObject.put("UserID", EMPLOYEE_ID);
            jsonObject.put("Username", sharedPreference.getValue(Constants.Preferred_Username));
            jsonObject.put("Password", sharedPreference.getValue(Constants.Preferred_Password));
            jsonObject.put("ImageData", Constants.GetBase64(ImageData));

            HTTPAsyncTask asyncTask = new HTTPAsyncTask(SendMail.this, 0, callback, null, jsonObject, null, "POST");
            asyncTask.execute(API_LOAD_CLASSES);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void SendMailDetails()
    {

        try {

            content.append("<b>Hello,</b></br>");
            content.append("This mail is regarding the enquiry/need of the product.</br>");
            content.append("Here in below attached the product details for your perusal.</br>");
            content.append("</br>");
            content.append(productStringBuilder+"</br>");
            content.append("</br>");
            content.append("No of quantity needed: "+ edtQunatity.getText());
            content.append("</br>");
            content.append("******************");
            content.append("</br>");
            content.append("Thanks & Regards</br>");
            content.append(EMPLOYEE_NAME);


            FROM = getString(R.string.FROM);
            SERVER_USERNAME  = getString(R.string.SERVER_USERNAME);
            SERVER_PASSWORD  = getString(R.string.SERVER_PASSWORD);
            TO = "support@progresys.com";
            SUBJECT = "Reg: Product Enquiry | Progresys ME GmbH | "+ Constants.GetCurrentDate();
            MESSAGE = content.toString();


            String API_LOAD_CLASSES = Constants.APPLICATION_API + "mailsender/sendEmail";
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("username", SERVER_USERNAME);
            jsonObject.put("password", SERVER_PASSWORD);
            jsonObject.put("from", FROM);
            jsonObject.put("to", TO);
            jsonObject.put("subject", SUBJECT);
            jsonObject.put("message", MESSAGE);


            HTTPAsyncTask asyncTask = new HTTPAsyncTask(SendMail.this, 1, callback, null, jsonObject, null, "POST");
            asyncTask.execute(API_LOAD_CLASSES);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    LocalSharedPreference sharedPreference;

    private void SendEnquiryDetails() {


        try {

            String API_LOAD_CLASSES = Constants.APPLICATION_API + "PostDetails/postEnquiryDetails";
            JSONObject jsonObject = new JSONObject();

            Constants.Logger(PRODUCT_ID+"/"+EMPLOYEE_ID, SendMail.this);
            jsonObject.put("ProductID", PRODUCT_ID);
            jsonObject.put("UserID", EMPLOYEE_ID);
            jsonObject.put("Quantity", edtQunatity.getText());
            jsonObject.put("Username", sharedPreference.getValue(Constants.Preferred_Username));
            jsonObject.put("Password", sharedPreference.getValue(Constants.Preferred_Password));
            HTTPAsyncTask asyncTask = new HTTPAsyncTask(SendMail.this, 2, callback, null, jsonObject, null, "POST");
            asyncTask.execute(API_LOAD_CLASSES);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    final CallBack callback = new CallBack() {
        @Override
        public void onProgress() {

        }

        @Override
        public void onResult(String result) {

        }

        @Override
        public void onResultExtra(String result, int id) throws JSONException {

            JSONObject object;

            if(id==1)//send mail
            {
                Constants.Logger("Mail: "+result, SendMail.this);

                object = new JSONObject(result);

            }else//send data enquiry
            {
                try {

                    Constants.Logger("Enquiry: "+result, SendMail.this);

                    object = new JSONObject(result);
                    String Str_Results = object.optString("Results");
                    String Str_Message = object.optString("Message");
                    JSONArray jsonArr = new JSONArray(Str_Results);

                    Constants.Logger(result, SendMail.this);

                    if (jsonArr.length() == 0 && Str_Message.equalsIgnoreCase("Failed")) {

                        Constants.SnackBar(SendMail.this, getString(R.string.label_noresult), parent, 2);

                    } else {

                        Constants.Logger("Data sent to server..", SendMail.this);

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

        @Override
        public void onCancel() {


        }
    };

}//END
//***************************************************************************************
