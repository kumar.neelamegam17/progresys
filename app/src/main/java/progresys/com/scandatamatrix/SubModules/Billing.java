package progresys.com.scandatamatrix.SubModules;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.widget.NestedScrollView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import progresys.com.scandatamatrix.Utils.LocalSharedPreference;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import progresys.com.scandatamatrix.CoreModules.Constants;
import progresys.com.scandatamatrix.Model.OptionList;
import progresys.com.scandatamatrix.NetworkRetro.ApiService;
import progresys.com.scandatamatrix.NetworkRetro.Model.Model_Category;
import progresys.com.scandatamatrix.NetworkRetro.Model.RequestModel;
import progresys.com.scandatamatrix.NetworkUtils.CallBack;
import progresys.com.scandatamatrix.NetworkUtils.HTTPAsyncTask;
import progresys.com.scandatamatrix.R;
import progresys.com.scandatamatrix.Utils.Application;
import progresys.com.scandatamatrix.Utils.ImagePickerActivity;
import progresys.com.scandatamatrix.Utils.Validations;

import static com.sun.mail.util.ASCIIUtility.getBytes;

public class Billing extends Application {

    @BindView(R.id.nested_scroll_view)
    NestedScrollView nestedScrollView;
    @BindView(R.id.img_photos)
    ImageView imgPhotos;
    @BindView(R.id.lyt_form)
    LinearLayout lytForm;
    @BindView(R.id.edt_price)
    AppCompatEditText edtPrice;
    @BindView(R.id.btn_options)
    FloatingActionButton btnOptions;
    @BindView(R.id.spn_category)
    Button Spn_Category;
    @BindView(R.id.spn_paymentmode)
    Button Spn_PaymentMode;
    @BindView(R.id.parent_coordinatorlayout)
    LinearLayout parent;

    public static final int REQUEST_IMAGE = 100;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    ArrayList<OptionList> categoryList = new ArrayList<>();
    ArrayList<OptionList> paymentModes = new ArrayList<>();

    ArrayList<String> categoryArrayList = new ArrayList<>();
    ArrayList<String> paymentModesArrayList = new ArrayList<>();

    String[] arrayCategory;
    String[] arrayPaymentMode;
    byte[] imageData;


    LocalSharedPreference sharedPreference;

    @BindView(R.id.label_bill) TextView labelBill;
    @BindView(R.id.label_category) TextView labelCategory;
    @BindView(R.id.label_paymentmode) TextView labelPaymentmode;
    @BindView(R.id.label_price) TextView labelPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_billing);

        try {
            GetInitialize();
            Controllisteners();

            //apiService = ApiClient.getClient(getApplicationContext()).create(ApiService.class);
            //GetMasterCategory();

            getMasterData();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void getMasterData() {

        try {

            categoryList = new ArrayList<>();
            paymentModes = new ArrayList<>();


            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Username", sharedPreference.getValue(Constants.Preferred_Username));
            jsonObject.put("Password", sharedPreference.getValue(Constants.Preferred_Password));

            try {

                String API_LOAD_CLASSES = Constants.APPLICATION_API + "getdetails/getBillCategory";

                HTTPAsyncTask asyncTask = new HTTPAsyncTask(Billing.this, 1, master_callback, null, jsonObject, null, "POST");
                asyncTask.execute(API_LOAD_CLASSES);

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                jsonObject = new JSONObject();
                jsonObject.put("Username", sharedPreference.getValue(Constants.Preferred_Username));
                jsonObject.put("Password", sharedPreference.getValue(Constants.Preferred_Password));
                jsonObject.put("UserID", EMPLOYEE_ID);

                String API_LOAD_CLASSES = Constants.APPLICATION_API + "getdetails/getPaymentMode";
                HTTPAsyncTask asyncTask = new HTTPAsyncTask(Billing.this, 2, master_callback, null, jsonObject, null, "POST");
                asyncTask.execute(API_LOAD_CLASSES);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    CallBack master_callback = new CallBack() {
        @Override
        public void onProgress() {

        }

        @Override
        public void onResult(String result) {

        }

        @Override
        public void onResultExtra(String result, int id) {
            try {

                JSONObject object = new JSONObject(result);
                String Str_Results = object.optString("Results");
                String Str_Message = object.optString("Message");
                JSONArray jsonArr = new JSONArray(Str_Results);

                //Constants.Logger(result, Details.this);

                if (jsonArr.length() == 0 && Str_Message.equalsIgnoreCase("Failed")) {

                } else {

                    if (jsonArr.length() > 0) {

                        for (int i = 0; i < jsonArr.length(); i++) {
                            JSONObject jsonObj = jsonArr.getJSONObject(i);

                            if (id == 1)//getBillCategory
                            {
                                int Id = jsonObj.getInt("Id");
                                String Data = jsonObj.getString("Category");

                                categoryList.add(new OptionList(Id, Data));
                                categoryArrayList.add(Data);
                                //arrCategory.put(Id, Data);

                            } else if (id == 2)//getPaymentMode
                            {
                                int Id = jsonObj.getInt("Id");
                                String Data = jsonObj.getString("PaymentModeName");

                                paymentModes.add(new OptionList(Id, Data));
                                paymentModesArrayList.add(Data);
                                //arrPaymentmode.put(Id, Data);
                            }

                            /*for (Iterator<String> it = jsonObj.keys(); it.hasNext(); ) {
                                Object key = it.next();
                                //based on you key types
                                String keyStr = (String) key;
                                Object keyvalue = jsonObj.get(keyStr);

                                //Print key and value
                                  Constants.Logger("key: "+ keyStr + " value: " + keyvalue , Billing.this);


                            }*/
                        }

                    } else {
                        Constants.SnackBar(Billing.this, "Failed to load category", parent, 2);
                    }


                }

                //Load both choice dialog
                arrayCategory = categoryArrayList.toArray(new String[categoryArrayList.size()]);
                arrayPaymentMode = paymentModesArrayList.toArray(new String[paymentModesArrayList.size()]);



            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onCancel() {


        }
    };


    private void GetMasterCategory() {

        try {

            RequestModel requestModel = new RequestModel();
            requestModel.setUsername(sharedPreference.getValue(Constants.Preferred_Username));
            requestModel.setPassword(sharedPreference.getValue(Constants.Preferred_Password));

            disposable.add(
                    apiService.getMasterCategory(requestModel)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .map(new Function<List<Model_Category>, List<Model_Category>>() {
                                @Override
                                public List<Model_Category> apply(List<Model_Category> notes) throws Exception {
                                    // TODO - note about sort
                                    Collections.sort(notes, new Comparator<Model_Category>() {
                                        @Override
                                        public int compare(Model_Category n1, Model_Category n2) {
                                            return n2.getId() - n1.getId();
                                        }
                                    });
                                    return notes;
                                }
                            })
                            .subscribeWith(new DisposableSingleObserver<List<Model_Category>>() {
                                @Override
                                public void onSuccess(List<Model_Category> notes) {
                                    categoryList.clear();
                                    // categoryList.addAll(notes);

                                    arrayCategory = categoryList.toArray(new String[categoryList.size()]);

                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.e("TEST RETRO", "onError: " + e.getMessage());

                                }
                            })
            );
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void Controllisteners() {

        btnOptions.setOnClickListener(v -> Dexter.withActivity(Billing.this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        } else {
                            // TODO - handle permission denied case
                            Constants.SnackBar(Billing.this, "Enable all permission from app settings", parent, 2);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check());

    }

    private void GetInitialize() {
        ButterKnife.bind(this);
        Constants.changeStatusBarColour(this);

        // Clearing older images from cache directory
        // don't call this line if you want to choose multiple images in the same activity
        // call this once the bitmap(s) usage is over
        ImagePickerActivity.clearCache(this);

        sharedPreference = new LocalSharedPreference(Billing.this);

        EMPLOYEE_ID = sharedPreference.getValue("EMPLOYEE_ID");

        Constants.HighlightMandatory("Attach Bill", labelBill);
        Constants.HighlightMandatory("Category", labelCategory);
        Constants.HighlightMandatory("Payment mode", labelPaymentmode);
        Constants.HighlightMandatory("Price", labelPrice);


    }

    @OnClick(R.id.spn_category)
    void onSpnCategoryClick() {


        Constants.showChoiceDialog(Spn_Category, arrayCategory);
    }

    @OnClick(R.id.spn_paymentmode)
    void onSpnPaymentmodeClick() {
        Constants.showChoiceDialog(Spn_PaymentMode, arrayPaymentMode);

    }

    @OnClick(R.id.btn_submit)
    void onBtnSubmitClick() {

        //TODO get all data and send to server
        if (CheckValidation()) {
            SendBillingInfo();
        } else {

            Constants.AlertDialog(this, "Fill mandatory fields *", errorMessage.toString(), R.drawable.ic_warning_black_24dp, R.color.red_500, false);
        }

    }

    StringBuilder errorMessage =new StringBuilder();
    public boolean CheckValidation() {

        errorMessage =new StringBuilder();

        boolean status = true;

        if(imageData== null || imageData.length==0)
        {
            status=false;
            errorMessage.append("Attach bill copy\n");
        }

        if (Spn_Category.getText().toString().equalsIgnoreCase("Select")) {
            status = false;
            errorMessage.append("Select category\n");
        }


        if (Spn_PaymentMode.getText().toString().equalsIgnoreCase("Select")) {
            status = false;
            errorMessage.append("Select payment mode\n");
        }


        if(Validations.CheckLength(edtPrice) || Integer.parseInt(edtPrice.getText().toString())==0)
        {
            status = false;
            errorMessage.append("Enter price\n");
        }



        return status;

    }

    String EMPLOYEE_ID;

    public void SendBillingInfo() {
        try {


            String API_LOAD_CLASSES = Constants.APPLICATION_API + "PostDetails/postBillDetails";
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("ImageData", Constants.GetBase64(imageData));
            jsonObject.put("Category", Spn_Category.getText());
            jsonObject.put("CategoryId", Constants.GetIdFromList(Spn_Category.getText(), categoryList));
            jsonObject.put("PaymentMode", Spn_PaymentMode.getText());
            jsonObject.put("PaymentModeId", Constants.GetIdFromList(Spn_PaymentMode.getText(), paymentModes));
            jsonObject.put("Price", edtPrice.getText());
            jsonObject.put("CreatedDate", Constants.GetTimeStamp());
            jsonObject.put("UserId", EMPLOYEE_ID);
            jsonObject.put("Username", sharedPreference.getValue(Constants.Preferred_Username));
            jsonObject.put("Password", sharedPreference.getValue(Constants.Preferred_Password));
            HTTPAsyncTask asyncTask = new HTTPAsyncTask(Billing.this, 0, callback, null, jsonObject, null, "POST");
            asyncTask.execute(API_LOAD_CLASSES);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    final CallBack callback = new CallBack() {
        @Override
        public void onProgress() {

        }

        @Override
        public void onResult(String result) {
            try {

                Constants.Logger(result,Billing.this);
                if(result!=null && result.length()>2)
                {

                }
                JSONObject object = new JSONObject(result);
                String Str_Results = object.optString("Results");
                String Str_Message = object.optString("Message");
                JSONArray jsonArr = new JSONArray(Str_Results);

                Constants.Logger(result, Billing.this);

                if (jsonArr.length() == 0 && Str_Message.equalsIgnoreCase("Failed")) {

                    Constants.SnackBar(Billing.this, getString(R.string.label_noresult), parent, 2);

                } else {
                    //Constants.SnackBar(Billing.this, "Submitted successfully..", parent, 2);

                    Constants.Logger("Data sent to server..", Billing.this);
                    Constants.AlertDialog(Billing.this, "Information", "Submitted successfully..",
                            R.drawable.ic_done_all_black_24dp, R.color.green_500, true);

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onResultExtra(String result, int id) {


        }

        @Override
        public void onCancel() {


        }
    };

    @Override
    public void onBackPressed() {
        Constants.OnBackPressedFromSubModules(this);
    }


    /**
     * Image picker and gallery
     */
    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                Constants.launchCameraIntent(Billing.this, false, false);
            }

            @Override
            public void onChooseGallerySelected() {
                Constants.launchGalleryIntent(Billing.this, false, true);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                try {
                    // loading profile image from local cache
                    loadImages(uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void loadImages(Uri uri) throws IOException {
        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
        imgPhotos.setImageBitmap(bitmap);

        InputStream iStream =   getContentResolver().openInputStream(uri);
        imageData = getBytes(iStream);

    }


}//End
