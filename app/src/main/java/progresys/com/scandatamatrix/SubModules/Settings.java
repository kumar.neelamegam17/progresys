package progresys.com.scandatamatrix.SubModules;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.easing.linear.Linear;

import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import progresys.com.scandatamatrix.CoreModules.Constants;
import progresys.com.scandatamatrix.CoreModules.Splash;
import progresys.com.scandatamatrix.R;
import progresys.com.scandatamatrix.Utils.Application;
import progresys.com.scandatamatrix.Utils.LocalSharedPreference;

/**
 * Created by Muthukumar Neelamegam on 3/26/2019.
 * progresys
 */
public class Settings extends Application {


    @BindView(R.id.label_ipaddress)
    TextView label_ipaddress;
    @BindView(R.id.edt_ipaddress)
    EditText edtIpaddress;
    @BindView(R.id.Parent)
    LinearLayout parent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_settings);

            GetInitialize();

    }


    LocalSharedPreference localSharedPreference;
    private void GetInitialize()
    {

        ButterKnife.bind(this);

        Constants.HighlightMandatory("Set IP address", label_ipaddress);
        localSharedPreference=new LocalSharedPreference(Settings.this);


        String CurrentIP = localSharedPreference.getValue(Constants.SharedPref_IPADDRESS);
        edtIpaddress.setText(CurrentIP);
        edtIpaddress.setFilters(Constants.ipaddressfilter());



    }



    @OnClick(R.id.btn_cancel) void onBtnCancelClick() {
        finish();
    }



    @OnClick(R.id.btn_save) void onBtnSaveClick() {

        if (CheckIpAddress()) {

            if (CheckValidation()) {

                localSharedPreference.setValue("IPADDRESS", edtIpaddress.getText().toString());

                Constants.AlertDialog(Settings.this, "Information", "Submitted successfully..",
                        R.drawable.ic_done_all_black_24dp, R.color.green_500, true);


            }else {

                Constants.AlertDialog(this, "Fill mandatory fields *", errorMessage.toString(), R.drawable.ic_warning_black_24dp, R.color.red_500, false);
            }


        }
        else
        {
            edtIpaddress.setText("");
            Constants.SnackBar(Settings.this, "Invalid IP address", parent, 2);
        }


    }



    boolean serverStatus=false;
    private boolean CheckIpAddress() {
        serverStatus = false;
        try {
            serverStatus = new Constants.PingTask().execute(Constants.IPADDRESS, String.valueOf(Constants.PORT)).get();

            return serverStatus;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return serverStatus;
    }



    StringBuilder errorMessage =new StringBuilder();
    public boolean CheckValidation()
    {
        boolean status=true;
        if(edtIpaddress== null || edtIpaddress.getText().length()==0)
        {
            status=false;
            errorMessage.append("Enter Ip Address\n");
        }
        return status;
    }



}//END
