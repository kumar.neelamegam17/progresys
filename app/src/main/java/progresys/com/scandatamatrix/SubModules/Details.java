package progresys.com.scandatamatrix.SubModules;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import progresys.com.scandatamatrix.Utils.LocalSharedPreference;
import progresys.com.scandatamatrix.CoreModules.Constants;
import progresys.com.scandatamatrix.CoreModules.Login;
import progresys.com.scandatamatrix.NetworkUtils.CallBack;
import progresys.com.scandatamatrix.NetworkUtils.HTTPAsyncTask;
import progresys.com.scandatamatrix.R;
import progresys.com.scandatamatrix.Utils.CustomDialog;
import progresys.com.scandatamatrix.Utils.ImagePickerActivity;

import static com.sun.mail.util.ASCIIUtility.getBytes;

public class Details extends AppCompatActivity {


    @BindView(R.id.txtvw_productname)
    TextView txtvw_Result;
    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.fabmail)
    FloatingActionButton floatingMailActionButton;

    @BindView(R.id.parent_coordinatorlayout)
    CoordinatorLayout parent_coordinatorlayout;
    @BindView(R.id.layout_dynamicdata)
    LinearLayout dynamicLayout;
    @BindView(R.id.img)
    ImageView Warehouseimg;


    byte[] imageData;
    boolean ImageChanged;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_details);


        try {
            GetInitialize();

            Controllisterners();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void Controllisterners() {


        //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show());

                floatingActionButton.setOnClickListener(v -> Dexter.withActivity(Details.this)
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    showImagePickerOptions();
                                } else {
                                    // TODO - handle permission denied case
                                    Constants.SnackBar(Details.this, "Enable all permission from app settings", parent_coordinatorlayout, 2);
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check());




        floatingMailActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 SendMail();
            }
        });


    }

    String scanned_result = "";

    private void GetInitialize() {

        ButterKnife.bind(this);
        sharedPreference = new LocalSharedPreference(Details.this);



        Constants.changeStatusBarColour(this);

        //floatingActionButton.setVisibility(View.INVISIBLE);

        if (getIntent().getExtras() != null) {
            Bundle retain_string = getIntent().getExtras();
            scanned_result = retain_string.getString("RESULT");

        }


        LoadDataFromServer();

        // Clearing older images from cache directory
        // don't call this line if you want to choose multiple images in the same activity
        // call this once the bitmap(s) usage is over
        ImagePickerActivity.clearCache(this);





    }

    private void LoadDataFromServer() {
        if (Constants.CheckNetwork(Details.this)) {
            LoadInformation(scanned_result);

        } else {
            ShowInternetDialog();
        }
    }

    private void ShowInternetDialog() {

        final Dialog dialog = new Dialog(Details.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.activity_no_item_internet_image);

        ProgressBar progress_bar;
        LinearLayout lyt_no_connection;
        AppCompatButton bt_retry;

        progress_bar = dialog.findViewById(R.id.progress_bar);
        lyt_no_connection = dialog.findViewById(R.id.lyt_no_connection);
        bt_retry = (AppCompatButton) dialog.findViewById(R.id.bt_retry);

        progress_bar.setVisibility(View.GONE);
        lyt_no_connection.setVisibility(View.VISIBLE);

        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                progress_bar.setVisibility(View.VISIBLE);
                lyt_no_connection.setVisibility(View.GONE);


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (Constants.CheckNetwork(Details.this)) {
                            dialog.dismiss();
                            LoadInformation(scanned_result);

                        } else {
                            progress_bar.setVisibility(View.GONE);
                            lyt_no_connection.setVisibility(View.VISIBLE);
                        }
                    }
                }, 1000);
            }
        });

        dialog.show();

    }


    LocalSharedPreference sharedPreference;


    private void LoadInformation(String PassKey) {

        try {

            String API_LOAD_CLASSES = Constants.APPLICATION_API + "getdetails/GetProductDetails";
            JSONObject jsonObject = new JSONObject();
            //jsonObject.put("ProductID", "PSMEMBWSNBxx01");
            jsonObject.put("Username",Constants.Preferred_Username);
            jsonObject.put("Password", Constants.Preferred_Password);
            jsonObject.put("ProductID", PassKey);
            sharedPreference.setValue("PRODUCT_ID", PassKey);
            jsonObject.put("Username", sharedPreference.getValue(Constants.Preferred_Username));
            jsonObject.put("Password", sharedPreference.getValue(Constants.Preferred_Password));

            HTTPAsyncTask asyncTask = new HTTPAsyncTask(Details.this, 0, callback, null, jsonObject, null, "POST");
            asyncTask.execute(API_LOAD_CLASSES);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void SendMail() {

        try {
            Bundle b = new Bundle();
            b.putString("DATA_INFO", PassResult);
            b.putByteArray("DATA_IMAGE", imageData);
            b.putBoolean("IMAGE_UPDATED", ImageChanged);
            Constants.globalStartIntent2(Details.this, SendMail.class, b);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);

        //menu.getItem(0).setIcon(R.drawable.ic_account_circle_black_24dp);
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {

            Constants.globalStartIntent(Details.this, ScanDataMatrix.class, null, 2);

        }
        //noinspection SimplifiableIfStatement

        if (id == R.id.action_refresh) {

            item.setVisible(false);
            //ShowUserinfo();
            return true;
        } else if (id == R.id.action_exit) {

            Constants.ExitDialog(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void ShowUserinfo() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.activity_userprofile);
        TextView EmployeeName, EmployeeDesignation;
        ImageButton exit, close;

        EmployeeName=dialog.findViewById(R.id.employee_name);
        EmployeeDesignation=dialog.findViewById(R.id.employee_destination);

        EmployeeName.setText(sharedPreference.getValue("EMPLOYEE_NAME"));
        EmployeeDesignation.setText(sharedPreference.getValue("EMPLOYEE_ID"));

        exit = dialog.findViewById(R.id.button_exit);
        close = dialog.findViewById(R.id.button_close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CustomDialog customDialog = new CustomDialog(Details.this);
                customDialog.setLayoutColor(R.color.red_400);
                customDialog.setImage(R.drawable.ic_exit_to_app_black_24dp);
                customDialog.setTitle("Information");
                customDialog.setDescription("Are you sure want to signout?");
                customDialog.setNegativeButtonTitle("No");
                customDialog.setPossitiveButtonTitle("Yes");
                customDialog.setOnPossitiveListener(new CustomDialog.possitiveOnClick() {
                    @Override
                    public void onPossitivePerformed() {
                        dialog.dismiss();
                        Constants.globalStartIntent(Details.this, Login.class, null, 2);

                    }
                });
                customDialog.show();

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.show();
    }

    BottomSheetDialog dialog;
    StringBuilder str_pass = new StringBuilder();

    String PassResult;

    final CallBack callback = new CallBack() {
        @Override
        public void onProgress() {

            View view = getLayoutInflater().inflate(R.layout.fragment_bottom_sheet_dialog, null);
            dialog = new BottomSheetDialog(Details.this);
            dialog.setContentView(view);
            dialog.setCancelable(false);
            dialog.show();

        }

        @Override
        public void onResult(String result) {
            try {
                Constants.Logger(result,Details.this);
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();

                }
                JSONObject object = new JSONObject(result);
                String Str_Results = object.optString("Results");
                String Str_Message = object.optString("Message");
                JSONArray jsonArr = new JSONArray(Str_Results);

                //Constants.Logger(result, Details.this);

                if (jsonArr.length() == 0 && Str_Message.equalsIgnoreCase("Failed")) {

                    Constants.SnackBar(Details.this, getString(R.string.label_noresult), parent_coordinatorlayout, 2);
                    txtvw_Result.setText(R.string.label_noproduct);

                } else {

                    PassResult = Str_Results;
                    if (jsonArr.length() > 0) {

                        for (int i = 0; i < jsonArr.length(); i++) {
                            JSONObject jsonObj = jsonArr.getJSONObject(i);

                            for (Iterator<String> it = jsonObj.keys(); it.hasNext(); ) {
                                Object key = it.next();
                                //based on you key types
                                String keyStr = (String) key;
                                Object keyvalue = jsonObj.get(keyStr);

                                if(keyStr.equals("ImageData")) //set image view
                                {
                                    if ((keyvalue != null && keyvalue.toString().length() > 0)) {
                                        Warehouseimg.setImageBitmap(Constants.getBase64toBitmap(keyvalue.toString(), Details.this));
                                    } else {
                                        Warehouseimg.setImageBitmap(Constants.getdefaultWarningImage(Details.this));
                                    }

                                }else //set textview
                                {
                                    DynamicLayout(keyStr, keyvalue.toString());

                                }


                            }
                        }

                        floatingActionButton.setVisibility(View.VISIBLE);

                    } else {
                        Constants.SnackBar(Details.this, getString(R.string.label_nodata), parent_coordinatorlayout, 2);
                    }


                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onResultExtra(String result, int id) {


        }

        @Override
        public void onCancel() {


        }
    };


    public void DynamicLayout(String Label, String Value) {
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_item_dynamic, null);
        TextView txtvw1, txtvw2;
        txtvw1 = view.findViewById(R.id.txtvw1);
        txtvw2 = view.findViewById(R.id.txtvw2);
        txtvw1.setText(Constants.CheckString(Label));
        txtvw2.setText(Constants.CheckString(Value));
        dynamicLayout.addView(view);
    }

    //**********************************************************************************************

    @Override
    public void onBackPressed() {

        Constants.globalStartIntent(Details.this, ScanDataMatrix.class, null, 2);

    }

    //**********************************************************************************************




    /**
     * Image picker and gallery
     */
    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                Constants.launchCameraIntent(Details.this, false, false);
            }

            @Override
            public void onChooseGallerySelected() {
                Constants.launchGalleryIntent(Details.this, false, true);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constants.REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                try {
                    // loading profile image from local cache
                    loadImages(uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void loadImages(Uri uri) throws IOException {
        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
        Warehouseimg.setImageBitmap(bitmap);

        InputStream iStream =   getContentResolver().openInputStream(uri);
        imageData = getBytes(iStream);
        ImageChanged=true;//notify the image changes
        //Toast.makeText(this, "Image updated..", Toast.LENGTH_SHORT).show();
    }


    //**********************************************************************************************


}//end
