package progresys.com.scandatamatrix.CoreModules;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.ButterKnife;
import progresys.com.scandatamatrix.R;
import progresys.com.scandatamatrix.SubModules.Billing;
import progresys.com.scandatamatrix.SubModules.ScanDataMatrix;
import progresys.com.scandatamatrix.SubModules.Settings;
import progresys.com.scandatamatrix.SubModules.InvoiceItems;
import progresys.com.scandatamatrix.Utils.Application;

/**
 * Created by Muthukumar Neelamegam on 3/25/2019.
 * Datamatrix
 */
public class Dashboard extends Application {


    @BindView(R.id.cardview_scan)
    CardView cardviewScan;
    @BindView(R.id.cardview_bill) CardView cardviewBill;
    @BindView(R.id.cardview_settings) CardView cardviewSettings;
    @BindView(R.id.cardview_warehouse) CardView cardviewWarehouse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_dashboard);


        try {
            GetInitialize();

            Controllisterners();

            LoadDashboard();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void Controllisterners() {

        cardviewScan.setOnClickListener(v -> Constants.globalStartIntent2(Dashboard.this, ScanDataMatrix.class, null));

        cardviewBill.setOnClickListener(v -> Constants.globalStartIntent2(Dashboard.this, Billing.class, null));

        cardviewSettings.setOnClickListener(v -> Constants.globalStartIntent2(Dashboard.this, Settings.class, null));

        cardviewWarehouse.setOnClickListener(v -> Constants.globalStartIntent2(Dashboard.this,
                InvoiceItems.class, null));


    }

    private void GetInitialize() {

        ButterKnife.bind(this);

        Constants.changeStatusBarColour(this);


        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       // getSupportActionBar().setIcon(R.drawable.ic_menu_black_24dp);


    }

    public void LoadDashboard()
    {

    }

    @Override
    public void onBackPressed() {
        Constants.ExitDialog(this);
    }
}//END