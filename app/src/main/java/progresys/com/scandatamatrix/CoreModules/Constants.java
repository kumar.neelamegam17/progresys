package progresys.com.scandatamatrix.CoreModules;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;


import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import progresys.com.scandatamatrix.Model.OptionList;
import progresys.com.scandatamatrix.NetworkRetro.Model.Model_Category;
import progresys.com.scandatamatrix.NetworkRetro.Model.Model_PaymentMode;
import progresys.com.scandatamatrix.R;
import progresys.com.scandatamatrix.SubModules.Details;
import progresys.com.scandatamatrix.Utils.CustomDialog;
import progresys.com.scandatamatrix.Utils.CustomIntent;
import progresys.com.scandatamatrix.Utils.ImagePickerActivity;

import android.util.Base64;


/**
 * Constants class that holds all the static variables and methods
 * MUTHUKUMAR NEELAMEGAM
 * m.neelamegam@progresys.com
 */
public class Constants {


    /**
     * REST API STATIC LINKS
     */
    //public static String IPADDRESS ="192.168.1.109"; //Local
     // public static String IPADDRESS ="140.78.235.231"; //JKU
      public static String IPADDRESS; //JKU
    //public static String IPADDRESS ="213.239.193.218"; //Server
    public static int PORT = 5555;

    //public static String APPLICATION_API="http://"+Constants.IPADDRESS+":"+PORT+"/API/";
    //public static final String BASE_URL = "http://"+Constants.IPADDRESS+":"+PORT+"/";

    public static String APPLICATION_API="";
    public static String BASE_URL = "";


    public static final String Preferred_RememberMe_Status = "REMEMBER_ME";
    public static final String Preferred_Username = "USERNAME";
    public static final String Preferred_Password = "PASSWORD";
    public static final String Preferred_UserData = "USERDATA";

    public static final String SharedPref_IPADDRESS = "IPADDRESS";

    private static final Constants ourInstance = new Constants();

    static Constants getInstance() {
        return ourInstance;
    }

    private Constants() {

    }


    public static Bitmap getdefaultWarningImage(Context ctx)
    {
        Bitmap decodedByte;
        decodedByte = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_warning_black_24dp);

        return decodedByte;
    }

    public static String GetBase64(byte[] imageData)
    {
        String sTRBytes;
        if(imageData!=null && imageData.length>0){
            sTRBytes = Base64.encodeToString(imageData, Base64.DEFAULT);
        }else
        {
            sTRBytes="";
        }

        return sTRBytes;
    }

    public static Bitmap getBase64toBitmap(String str, Context ctx)
    {
        Bitmap decodedByte;
        decodedByte = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_warning_black_24dp);
        if(str.length()>0 && str!=null)
        {
            byte[] decodedString = Base64.decode(str, Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        }
        return decodedByte;
    }

    public static final int REQUEST_IMAGE = 100;

    public static void launchCameraIntent(Context ctx, boolean cropRatioLock, boolean fixedSize) {
        Intent intent = new Intent(ctx, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, cropRatioLock);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, fixedSize);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        ((Activity)ctx).startActivityForResult(intent, REQUEST_IMAGE);
    }

    public static void launchGalleryIntent(Context ctx, boolean cropRatioLock, boolean fixedSize)  {
        Intent intent = new Intent(ctx, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, cropRatioLock);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, fixedSize);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        ((Activity)ctx).startActivityForResult(intent, REQUEST_IMAGE);
    }


    public static void OnBackPressedFromSubModules(Context ctx)
    {
        Constants.globalStartIntent(ctx, Dashboard.class, null, 2);

    }

    public static void InitiateActionBar(Context ctx, String title, boolean backEnable)
    {

        androidx.appcompat.app.ActionBar toolbar = ((AppCompatActivity)ctx).getSupportActionBar();
        toolbar.setTitle(title);
        toolbar.setDisplayHomeAsUpEnabled(backEnable);
    }


    public enum ErrorStatus {
        NETWORK,
        NOTREACHABLE,
        VALIDATION
    }

    public static boolean CheckIsReachable() throws IOException, InterruptedException {
        Runtime runtime = Runtime.getRuntime();
        Process proc = runtime.exec("ping "+Constants.APPLICATION_API); //<- Try ping -c 1 www.serverURL.com
        int mPingResult = proc .waitFor();
        if(mPingResult == 0){

            return true;
        }else{

            return false;
        }
    }

    public static int GetIdFromList(CharSequence text, ArrayList<OptionList> optionLists)
    {
        int Id=0;
        for (OptionList optionList : optionLists) {
            if (optionList.getData().equals(text)) {
                Id = optionList.getId();
            }
        }
        return Id;
    }


    public static  void showChoiceDialog(final Button bt, String[] dataInput) {
        //String[] array_states = bt.getContext().getResources().getStringArray(R.array.sample);
        final AlertDialog.Builder builder = new AlertDialog.Builder(bt.getContext());
        builder.setCancelable(true);
        builder.setSingleChoiceItems(dataInput, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(dataInput[which]);
            }
        });
        builder.show();
    }


    public String GetEdittextValue(EditText edt)
    {
        String str="";
        if(edt.getText().length()==0 && edt!=null)
        {

        }
        return str;
    }

    public static InputFilter[] ipaddressfilter()
    {
        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end,
                                       android.text.Spanned dest, int dstart, int dend) {
                if (end > start) {
                    String destTxt = dest.toString();
                    String resultingTxt = destTxt.substring(0, dstart)
                            + source.subSequence(start, end)
                            + destTxt.substring(dend);
                    if (!resultingTxt
                            .matches("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) {
                        return "";
                    } else {
                        String[] splits = resultingTxt.split("\\.");
                        for (int i = 0; i < splits.length; i++) {
                            if (Integer.valueOf(splits[i]) > 255) {
                                return "";
                            }
                        }
                    }
                }
                return null;
            }

        };

        return filters;
    }

    public static String GetCurrentTime() {
        String str = null;
        try {

            str = "";
            SimpleDateFormat sdf4 = new SimpleDateFormat("h:mm a");
            String currentDateandTime = sdf4.format(new Date());     //8:29 PM
            str = currentDateandTime;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }


   public static boolean CheckNetwork(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static String CheckString(String str) {
        if (str == null) {
            return "";
        } else if (str.trim().equalsIgnoreCase("null") || str.equalsIgnoreCase("null")) {
            return "";
        } else if (str.length() > 0) {
            return str;
        } else {
            return "-";
        }

    }

    /**
     * Check if host is reachable.
     * @param host The host to check for availability. Can either be a machine name, such as "google.com",
     *             or a textual representation of its IP address, such as "8.8.8.8".
     * @param port The port number.
     * @param timeout The timeout in milliseconds.
     * @return True if the host is reachable. False otherwise.
     */
    public static boolean isHostAvailable(final String host, final int port, final int timeout) {
        try (final Socket socket = new Socket()) {
            final InetAddress inetAddress = InetAddress.getByName(host);
            final InetSocketAddress inetSocketAddress = new InetSocketAddress(inetAddress, port);

            socket.connect(inetSocketAddress, timeout);
            return true;
        } catch (java.io.IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void SnackBar(Context ctx, String Message, View parentLayout, int id) {

        Snackbar mSnackBar = Snackbar.make(parentLayout, Message, Snackbar.LENGTH_LONG);
        View view = mSnackBar.getView();
        view.setPadding(5, 5, 5, 5);

        if (id == 1)//Positive
        {
            view.setBackgroundColor(ctx.getResources().getColor(R.color.green_500));
        } else if (id == 2)//Negative
        {
            view.setBackgroundColor(ctx.getResources().getColor(R.color.md_deep_orange_300));
        } else//Negative
        {
            view.setBackgroundColor(ctx.getResources().getColor(R.color.md_deep_orange_300));
        }


        TextView mainTextView =  (view).findViewById(com.google.android.material.R.id.snackbar_text);
        mainTextView.setAllCaps(true);
        mainTextView.setTextSize(16);
        mainTextView.setTextColor(ctx.getResources().getColor(R.color.md_white_1000));
        mSnackBar.setDuration(3000);
        mSnackBar.show();




    }


    public static void Logger(String Message, Context ctx)
    {
        Log.e(ctx.getPackageCodePath(), Message);
    }



    public static String GetTimeStamp()
    {
        String str="";
        try {

            Long tsLong = System.currentTimeMillis()/1000;
            str = tsLong.toString();

            return str;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str;
    }

    public static String GetCurrentDate()
    {
        String date = new SimpleDateFormat("dd-MM-YYYY", Locale.getDefault()).format(new Date());
        return date;
    }


    public static void HighlightMandatory(String LabelName, TextView textview) {
        String starsymbol = "<font color='#EE0000'><b>*</b></font>";
        textview.setText(Html.fromHtml(LabelName + starsymbol));
    }


    public static void ExitDialog(Context mContext) {

        CustomDialog customDialog=new CustomDialog(mContext);
        customDialog.setLayoutColor(R.color.red_400);
        customDialog.setImage(R.drawable.ic_exit_to_app_black_24dp);
        customDialog.setTitle("Information");
        customDialog.setDescription("Are you sure want to exit?");
        customDialog.setNegativeButtonTitle("No");
        customDialog.setPossitiveButtonTitle("Yes");
        customDialog.setOnPossitiveListener(new CustomDialog.possitiveOnClick() {
            @Override
            public void onPossitivePerformed() {
                ((Activity) mContext).finishAffinity();
            }
        });
        customDialog.show();

    }

    public static void AlertDialog(Context mContext,String Title, String Message, int drawable, int color, boolean exit) {

        CustomDialog customDialog=new CustomDialog(mContext);
        customDialog.setLayoutColor(color);
        customDialog.setImage(drawable);
        customDialog.setTitle(Title);
        customDialog.setDescription(Message);
        customDialog.setNegativeButtonVisible(View.GONE);
        customDialog.setPossitiveButtonTitle("Ok");
        customDialog.setOnPossitiveListener(new CustomDialog.possitiveOnClick() {
            @Override
            public void onPossitivePerformed() {
                customDialog.dismiss();

                if (exit)
                ((Activity)mContext).finish();
            }
        });
        customDialog.show();

    }




    public static void animateTextView(int initialValue, int finalValue, final TextView textview) {
        DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator(0.8f);
        int start = Math.min(initialValue, finalValue);
        int end = Math.max(initialValue, finalValue);
        int difference = Math.abs(finalValue - initialValue);
        Handler handler = new Handler();
        for (int count = start; count <= end; count++) {
            int time = Math.round(decelerateInterpolator.getInterpolation((((float) count) / difference)) * 50) * count;
            final int finalCount = ((initialValue > finalValue) ? initialValue - count : count);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    textview.setText(String.valueOf(finalCount));
                }
            }, time);
        }
    }


    public static void globalStartIntent(Context context, Class classes, Bundle bundle, int id) {
        ((Activity) context).finish();
        Intent intent = new Intent(context, classes);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        CustomIntent.customType(context, id);
        context.startActivity(intent);

    }

    public static void globalStartIntent2(Context context, Class classes, Bundle bundle) {

        Intent intent = new Intent(context, classes);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        CustomIntent.customType(context, 1);
        context.startActivity(intent);

    }



    public static void changeStatusBarColour(Context ctx) {
        if (Build.VERSION.SDK_INT >= 21) {
            ((Activity) ctx).getWindow().setNavigationBarColor(ContextCompat.getColor(ctx, R.color.colorPrimaryDark)); // Navigation bar the soft bottom of some phones like nexus and some Samsung note series
            ((Activity) ctx).getWindow().setStatusBarColor(ContextCompat.getColor(ctx, R.color.colorPrimary)); //status bar or the time bar at the top
        }
    }


    public static class PingTask extends AsyncTask<String, Boolean, Boolean> {

        protected Boolean doInBackground(String... params) {
            String url = params[0];
            int port =  Integer.parseInt(params[1]);
            boolean success = false;

            try {
                success = isPortOpen(url, port, 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return success;
        }

        protected void onPostExecute(Boolean result) {
            // do something when a result comes from the async task.
            Log.e("Server Lookup:", String.valueOf(result));
            super.onPostExecute(result);

        }
    }



    public static boolean isPortOpen(final String ip, final int port, final int timeout) {

        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(ip, port), timeout);
            socket.close();
            return true;
        }

        catch(ConnectException ce){
            ce.printStackTrace();
            return false;
        }

        catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

}//END
