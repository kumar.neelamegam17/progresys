package progresys.com.scandatamatrix.CoreModules;

import android.os.Bundle;
import android.os.Handler;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.ExecutionException;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import progresys.com.scandatamatrix.Utils.LocalSharedPreference;
import progresys.com.scandatamatrix.R;
import progresys.com.scandatamatrix.Utils.CoreActivity;

public class Splash  extends CoreActivity {

    //***************************************************************************************
    //Declaration
    @BindView(R.id.txtvw_title)
    TextView txtvwTitle;
    @BindView(R.id.img_logo)
    ImageView imgLogo;
    @BindView(R.id.progressbar)
    ProgressBar progressbar;
    @BindView(R.id.layout_parent)
    RelativeLayout parent;


    private int progress = 0;
    private int progressStatus = 0;
    private final Handler handler = new Handler();

    //***************************************************************************************
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_splash);

        try {
            isStoragePermissionGranted();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    //***************************************************************************************
    @Override
    public void onPermissionsGranted(int requestCode) {

        try {

            GetInitialize();

        } catch (Exception e) {

            e.printStackTrace();
        }


    }

    //***************************************************************************************
    LocalSharedPreference sharedPreference;

    private void GetInitialize() {

        ButterKnife.bind(this);

        sharedPreference = new LocalSharedPreference(Splash.this);

        Constants.changeStatusBarColour(this);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                YoYo.with(Techniques.Landing)
                        .duration(1500)
                        .playOn(findViewById(R.id.img_logo));
            }
        });


        String ipstatus = "0";
        try {
            ipstatus = sharedPreference.getValue("IPADDRESS_SET");
            Constants.IPADDRESS = sharedPreference.getValue(Constants.SharedPref_IPADDRESS);
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (ipstatus==null || ipstatus.equals("0")) {
            ShowIpDialog();
        }else{
            CallNextIntent();
        }


    }



    private void ShowIpDialog() {

        AlertDialog alertDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.initdialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        EditText ipaddress =  dialogView.findViewById(R.id.edt_ipaddress);
        ipaddress.setText(Constants.IPADDRESS);
        ipaddress.setFilters(Constants.ipaddressfilter());

        Button Cancel, Save;
        Cancel = dialogView.findViewById(R.id.btn_cancel);
        Save = dialogView.findViewById(R.id.btn_save);


        alertDialog = dialogBuilder.create();
        alertDialog.show();


        AlertDialog finalAlertDialog1 = alertDialog;
        Save.setOnClickListener(v -> {
            Constants.IPADDRESS = ipaddress.getText().toString();

            if (CheckIpAddress()) {

                finalAlertDialog1.dismiss();
                CallNextIntent();
                //save it in shared preference
                sharedPreference.setValue("IPADDRESS_SET", "1");
                sharedPreference.setValue(Constants.SharedPref_IPADDRESS, ipaddress.getText().toString());
            }
            else
            {
                ipaddress.setText("");
                Constants.SnackBar(Splash.this, "Invalid IP address", parent, 2);
            }

        });


        AlertDialog finalAlertDialog = alertDialog;
        Cancel.setOnClickListener(v -> {
                finalAlertDialog.dismiss();
                finish();
        });



    }



    boolean serverStatus=false;
    private boolean CheckIpAddress() {
        serverStatus = false;
        try {
            serverStatus = new Constants.PingTask().execute(Constants.IPADDRESS, String.valueOf(Constants.PORT)).get();

            return serverStatus;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return serverStatus;
    }

    public boolean isConnectedToServer(String url, int timeout) {
        try{
            URL myUrl = new URL(url);
            URLConnection connection = myUrl.openConnection();
            connection.setConnectTimeout(timeout);
            connection.connect();
            Log.e("Connection Status: ", "true");
            return true;
        } catch (Exception e) {
            // Handle your exceptions
            Log.e("Connection Status: ", "false");
            return false;
        }
    }

//***************************************************************************************


    @Override
    protected void bindViews() {

    }

    @Override
    protected void setListeners() {

    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


//***************************************************************************************


    @Override
    public void onBackPressed() {

    }



//***************************************************************************************

    private void CallNextIntent() {

        new Thread(new Runnable() {
            public void run() {

                while (progressStatus < 100) {
                    progressStatus = doSomeWork();

                    handler.post(() -> progressbar.setProgress(progressStatus));
                }

                handler.post(() -> {

                    progressbar.setVisibility(View.GONE);
                    Constants.globalStartIntent(Splash.this, Login.class, null,1);

                });
            }

            private int doSomeWork() {
                try {

                    Thread.sleep(20L);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ++progress;
                return
                        progress;
            }
        }).start();

    }



//***************************************************************************************


}//END
