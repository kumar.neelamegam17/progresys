package progresys.com.scandatamatrix.NetworkRetro.Model;

public class RequestModel {

    String Username;
    String Password;

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
