package progresys.com.scandatamatrix.NetworkRetro;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Single;
import progresys.com.scandatamatrix.NetworkRetro.Model.Model_Billing;
import progresys.com.scandatamatrix.NetworkRetro.Model.Model_Category;
import progresys.com.scandatamatrix.NetworkRetro.Model.Model_PaymentMode;
import progresys.com.scandatamatrix.NetworkRetro.Model.RequestModel;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiService {

    // Register new user
    @FormUrlEncoded
    @POST("API/postdetails/postBillDetails")
    Single<Model_Billing> createBill();


    // Fetch all category

    @POST("API/getdetails/getBillCategory")
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    Single<List<Model_Category>> getMasterCategory(@Body RequestModel body);

    // Fetch all payment mode
    @POST("API/getdetails/getPaymentMode")
    Single<List<Model_PaymentMode>> getMasterPaymentMode(@Field("payment")String data);


    // Create note
   // @FormUrlEncoded
   // @POST("notes/new")
   // Single<Note> createNote(@Field("note") String note);

   /* // Fetch all notes
    @GET("notes/all")
    Single<List<Note>> fetchAllNotes();

    // Update single note
    @FormUrlEncoded
    @PUT("notes/{id}")
    Completable updateNote(@Path("id") int noteId, @Field("note") String note);

    // Delete note
    @DELETE("notes/{id}")
    Completable deleteNote(@Path("id") int noteId); */
}