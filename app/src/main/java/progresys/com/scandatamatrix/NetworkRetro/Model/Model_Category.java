package progresys.com.scandatamatrix.NetworkRetro.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import progresys.com.scandatamatrix.NetworkRetro.BaseResponse;

public class Model_Category extends BaseResponse {



    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public Model_Category(int id, String categoryName, boolean isActive) {
        Id = id;
        this.categoryName = categoryName;
        IsActive = isActive;
    }

    int Id;
    String categoryName;
    boolean IsActive;




}
