package progresys.com.scandatamatrix.NetworkRetro.Model;

import progresys.com.scandatamatrix.NetworkRetro.BaseResponse;

public class Model_PaymentMode extends BaseResponse {

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getPaymentModeName() {
        return PaymentModeName;
    }

    public void setPaymentModeName(String paymentModeName) {
        PaymentModeName = paymentModeName;
    }

    public int getUser_Id() {
        return User_Id;
    }

    public void setUser_Id(int user_Id) {
        User_Id = user_Id;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    int Id;
    String PaymentModeName;
    int User_Id;
    boolean IsActive;

    public Model_PaymentMode(int id, String paymentModeName, int user_Id, boolean isActive) {
        Id = id;
        PaymentModeName = paymentModeName;
        User_Id = user_Id;
        IsActive = isActive;
    }
}
