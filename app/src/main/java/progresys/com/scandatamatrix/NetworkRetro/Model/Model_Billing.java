package progresys.com.scandatamatrix.NetworkRetro.Model;

import java.util.List;

import progresys.com.scandatamatrix.NetworkRetro.BaseResponse;

public class Model_Billing extends BaseResponse {

    public Model_Billing(int id, List<String> photoUrl, String category, int categoryPosition, String paymentMode,
                         int paymentModePosition, String price, String createdDate,
                         int userId) {
        Id = id;
        PhotoUrl = photoUrl;
        Category = category;
        CategoryPosition = categoryPosition;
        PaymentMode = paymentMode;
        PaymentModePosition = paymentModePosition;
        Price = price;
        CreatedDate = createdDate;
        UserId = UserId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    int UserId;

    int Id;
    List<String> PhotoUrl;

    String Category;
    int CategoryPosition;

    String PaymentMode;
    int PaymentModePosition;

    String Price;

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    String CreatedDate;

    public boolean IsCategorySelected()
    {
        return CategoryPosition > 0;
    }

    public boolean IsPaymentModeSelected()
    {
        return PaymentModePosition > 0 ;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public List<String> getPhotoUrl() {
        return PhotoUrl;
    }

    public void setPhotoUrl(List<String> photoUrl) {
        PhotoUrl = photoUrl;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public int getCategoryPosition() {
        return CategoryPosition;
    }

    public void setCategoryPosition(int categoryPosition) {
        CategoryPosition = categoryPosition;
    }

    public String getPaymentMode() {
        return PaymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        PaymentMode = paymentMode;
    }

    public int getPaymentModePosition() {
        return PaymentModePosition;
    }

    public void setPaymentModePosition(int paymentModePosition) {
        PaymentModePosition = paymentModePosition;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }


}
