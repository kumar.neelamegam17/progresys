package progresys.com.scandatamatrix.NetworkRetro.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseClass {

    public List<Model_Category> getCategories() {
        return Categories;
    }

    public void setCategories(List<Model_Category> categories) {
        Categories = categories;
    }

    @SerializedName("Results")
    @Expose
    private List<Model_Category> Categories = null;


}
