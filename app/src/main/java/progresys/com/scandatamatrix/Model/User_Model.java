package progresys.com.scandatamatrix.Model;

public class User_Model {
    int Id;
    String UserName;
    String Password;
    String EmployeeName;
    String Deviceinfo;
    boolean IsActive;
    int IsUpdate;
    int EmployeeId;
    int DesignationId;
    int Access;
    String Email;
    String UserId;
    String Avatar;
    int Style;
    int Language;

    public User_Model(int id, String userName, String password, String employeeName, String deviceinfo, boolean isActive, int isUpdate, int employeeId, int designationId, int access, String email, String userId, String avatar, int style, int language) {
        Id = id;
        UserName = userName;
        Password = password;
        EmployeeName = employeeName;
        Deviceinfo = deviceinfo;
        IsActive = isActive;
        IsUpdate = isUpdate;
        EmployeeId = employeeId;
        DesignationId = designationId;
        Access = access;
        Email = email;
        UserId = userId;
        Avatar = avatar;
        Style = style;
        Language = language;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }

    public String getDeviceinfo() {
        return Deviceinfo;
    }

    public void setDeviceinfo(String deviceinfo) {
        Deviceinfo = deviceinfo;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public int getIsUpdate() {
        return IsUpdate;
    }

    public void setIsUpdate(int isUpdate) {
        IsUpdate = isUpdate;
    }

    public int getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(int employeeId) {
        EmployeeId = employeeId;
    }

    public int getDesignationId() {
        return DesignationId;
    }

    public void setDesignationId(int designationId) {
        DesignationId = designationId;
    }

    public int getAccess() {
        return Access;
    }

    public void setAccess(int access) {
        Access = access;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getAvatar() {
        return Avatar;
    }

    public void setAvatar(String avatar) {
        Avatar = avatar;
    }

    public int getStyle() {
        return Style;
    }

    public void setStyle(int style) {
        Style = style;
    }

    public int getLanguage() {
        return Language;
    }

    public void setLanguage(int language) {
        Language = language;
    }
}
