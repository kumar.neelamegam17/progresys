package progresys.com.scandatamatrix.Model;

import com.sun.mail.imap.protocol.ID;

import java.util.HashMap;

public class ProductDetails_Model {

    public HashMap<String, String> getResponseData() {
        return responseData;
    }

    public void setResponseData(HashMap<String, String> responseData) {
        this.responseData = responseData;
    }

    HashMap<String, String> responseData;



    public ProductDetails_Model(int ID, String dataMatrixCode, String title, String name, String description, String PId, String manufacturer, String serialNumber, String receiptDate, String thumbnail, String protocoll, boolean isActive, boolean isUpdate, String actDate, String additionalInfo) {
        this.ID = ID;
        DataMatrixCode = dataMatrixCode;
        Title = title;
        Name = name;
        Description = description;
        this.PId = PId;
        Manufacturer = manufacturer;
        SerialNumber = serialNumber;
        ReceiptDate = receiptDate;
        Thumbnail = thumbnail;
        Protocoll = protocoll;
        IsActive = isActive;
        IsUpdate = isUpdate;
        ActDate = actDate;
        AdditionalInfo = additionalInfo;
    }

    int ID;
    String DataMatrixCode;
    String Title;
    String Name;
    String Description;
    String PId;
    String Manufacturer;
    String SerialNumber;
    String ReceiptDate;
    String Thumbnail;
    String Protocoll;
    boolean IsActive;
    boolean IsUpdate;
    String ActDate;
    String AdditionalInfo;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getDataMatrixCode() {
        return DataMatrixCode;
    }

    public void setDataMatrixCode(String dataMatrixCode) {
        DataMatrixCode = dataMatrixCode;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getPId() {
        return PId;
    }

    public void setPId(String PId) {
        this.PId = PId;
    }

    public String getManufacturer() {
        return Manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        Manufacturer = manufacturer;
    }

    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        SerialNumber = serialNumber;
    }

    public String getReceiptDate() {
        return ReceiptDate;
    }

    public void setReceiptDate(String receiptDate) {
        ReceiptDate = receiptDate;
    }

    public String getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        Thumbnail = thumbnail;
    }

    public String getProtocoll() {
        return Protocoll;
    }

    public void setProtocoll(String protocoll) {
        Protocoll = protocoll;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public boolean isUpdate() {
        return IsUpdate;
    }

    public void setUpdate(boolean update) {
        IsUpdate = update;
    }

    public String getActDate() {
        return ActDate;
    }

    public void setActDate(String actDate) {
        ActDate = actDate;
    }

    public String getAdditionalInfo() {
        return AdditionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        AdditionalInfo = additionalInfo;
    }
}
