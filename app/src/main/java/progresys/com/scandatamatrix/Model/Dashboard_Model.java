package progresys.com.scandatamatrix.Model;

import android.graphics.drawable.Drawable;

public class Dashboard_Model {

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public Dashboard_Model(int id, String title, String description, boolean isActive, Drawable icon) {
        Id = id;
        Title = title;
        Description = description;
        IsActive = isActive;
        this.icon = icon;
    }

    int Id;
    String Title;
    String Description;
    boolean IsActive;
    Drawable icon;


}
