package progresys.com.scandatamatrix.Model;

public class OptionList {
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }

    int Id;
    String Data;

    public OptionList(int id, String data) {
        Id = id;
        Data = data;
    }
}
